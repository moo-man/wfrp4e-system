# Warhammer Fantasy Roleplay 4e System for Foundry VTT

## Installation Instructions

To install a system, follow these instructions:

1. [Download the zip file] (URL Pending) If one is not provided, the module is still in early development.
2. Extract the included folder to `public/systems` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop. 