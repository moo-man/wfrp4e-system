/**
 * Override and extend the basic :class:`Item` implementation
 */
class ItemWfrp4e extends Item {
    roll() {
      const data = {
        template: `public/systems/wfrp4e/templates/chat/${this.data.type}-card.html`,
        actor: this.actor,
        item: this.data,
        data: this[this.data.type+"ChatData"]()
      };
      renderTemplate(data.template, data).then(html => {
        ChatMessage.create({
          user: game.user._id,
          alias: this.actor.name,
          content: html
        }, true);
      });
    }
  
    /* -------------------------------------------- */
  
    weaponChatData() {
      return this.data.data;
    }

    skillChatData() {
      return this.data.data;
    }

    talentChatData() {
      return this.data.data;
    }
  
    /* -------------------------------------------- */
  
    backpackChatData() {
      return duplicate(this.data.data);
    }
  
    /* -------------------------------------------- */
  
    /**
     * Roll Attackers Opposed Melee Test
     */
    rollAttackersTest(ev) {
        
      // Get data
      let skl = this.actor.data.data.skills.meleeBasic.total,
          parts = ["1d100ms<=", skl],
          flavor = `${this.name} - Attackers Roll`;

      // Render modal dialog
      let template = "public/systems/wfrp4e/templates/chat/roll-dialog.html";
      console.log(ev);
      renderTemplate(template, {formula: parts.join("")}).then(dlg => {
        new Dialog({
          title: flavor,
          content: dlg,
          buttons: {
            advantage: {
              label: "Advantage",
              callback: () => {
                parts[0] = "2d20kh";
                flavor += " (Advantage)"
              }
            },
            normal: {
              label: "Normal",
            },
            disadvantage: {
              label: "Disadvantage",
              callback: () => {
                parts[0] = "2d20kl";
                flavor += " (Disadvantage)"
              }
            }
          },
          close: html => {
            
            new Roll(parts.join("")).toMessage({
              alias: this.actor.name,
              flavor: flavor
            });
          }
        }, { width: 400, top: ev.clientY - 80, left: window.innerWidth - 710 }).render(true);
      });
    }
  
    /* -------------------------------------------- */
   
    /**
     * Roll a Spell Attack
     */
    /* rollSpellAttack(ev) {
      if ( this.type !== "spell" ) throw "Wrong item type!";
      let ability = this.data.data.ability.value || this.actor.data.data.attributes.spellcasting.value;
  
      // Get data
      let abl = this.actor.data.data.abilities[ability],
        prof = this.actor.data.data.attributes.prof.value,
        parts = ["1d20", "@mod", "@prof", "@bonus"],
        flavor = `${this.name} - Spell Attack Roll`;
  
      // Render modal dialog
      let template = "public/systems/wfrp4e/templates/chat/roll-dialog.html";
      console.log(ev);
      renderTemplate(template, {formula: parts.join(" + ")}).then(dlg => {
        new Dialog({
          title: flavor,
          content: dlg,
          buttons: {
            advantage: {
              label: "Advantage",
              callback: () => {
                parts[0] = "2d20kh";
                flavor += " (Advantage)"
              }
            },
            normal: {
              label: "Normal",
            },
            disadvantage: {
              label: "Disadvantage",
              callback: () => {
                parts[0] = "2d20kl";
                flavor += " (Disadvantage)"
              }
            }
          },
          close: html => {
            let bonus = html.find('[name="bonus"]').val();
            new Roll(parts.join(" + "), {mod: abl.mod, prof: prof, bonus: bonus}).toMessage({
              alias: this.actor.name,
              flavor: flavor
            });
          }
        }, { width: 400, top: ev.clientY - 80, left: window.innerWidth - 710 }).render(true);
      });
    } */
  
    /* -------------------------------------------- */
  
    /**
     * Roll Spell Damage
     */
/*     rollSpellDamage(ev) {
      if ( this.type !== "spell" ) throw "Wrong item type!";
      let ability = this.data.data.ability.value || this.actor.data.data.attributes.spellcasting.value;
  
      // Get data
      let abl = this.actor.data.data.abilities[ability],
        dmg = this.data.data.damage.value,
        parts = [dmg, "@bonus"],
        flavor = `${this.name} - Damage Roll`;
  
      // Render modal dialog
      let template = "public/systems/wfrp4e/templates/chat/roll-dialog.html";
      renderTemplate(template, {formula: parts.join(" + ")}).then(dlg => {
        new Dialog({
          title: flavor,
          content: dlg,
          buttons: {
            advantage: {
              label: "Critical Hit",
              callback: () => {
                parts[0] = Roll.alter(dmg, 0, 2);
                flavor += " (Critical)"
              }
            },
            normal: {
              label: "Normal",
            },
          },
          close: html => {
            let bonus = html.find('[name="bonus"]').val();
            new Roll(parts.join(" + "), {mod: abl.mod, bonus: bonus}).toMessage({
              alias: this.actor.name,
              flavor: flavor
            });
          }
        }, { width: 400, top: ev.clientY - 80, left: window.innerWidth - 710 }).render(true);
      });
    } */
  
    /* -------------------------------------------- */
  
    static chatListeners(html) {
      console.log("item listener");
      // Chat card actions
      html.on('click', '.card-buttons button', ev => {
        ev.preventDefault();

        console.log("game: ", game);

        // Extract card data
        let button = $(ev.currentTarget),
            messageId = button.parents('.message').attr("data-message-id"),
            senderId = game.data.messages.find(m => m._id === messageId).user._id;
  
        // Confirm roll permission
        if ( !game.user.isGM && ( game.user._id !== senderId )) return;
  
        // Extract action data
        let action = button.attr("data-action"),
            card = button.parents('.chat-card'),
            actor = game.actors.get(card.attr('data-actor-id')),
            itemId = Number(card.attr("data-item-id"));
  
        // roll chat listener (this will need to be tidied up)
        if ( action === "fortune-reroll" ) {

          if (actor.data.data.status.fortune.value > 0) {

            // reduce actor fortune by one
            actor.data.data.status.fortune.value = actor.data.data.status.fortune.value -1;

            // derive data to return to actor.rollSimpleTest method.
            let statId = card.attr('data-stat-id'),
                statType = card.attr('data-stat-type'),
                difficulty = card.attr('data-difficulty-id'),
                rollModifier = card.attr('roll-modifier'),
                result = button.attr("data-result");

            let reRenderData = {
              messageId: messageId,
              difficulty: difficulty,          
              actor: actor,
              action: action,
              result: result,
              rollModifier: rollModifier,
              fortune: true,
              reroll: true,
              addSuccessLevel: false
            }

            console.log("statType: ", statType);
            console.log("statId: ", statId);
            console.log("reRenderData: ", reRenderData);
            
            actor.rollSimpleTest(statType, statId, true, reRenderData);
          } else {
            console.log("no fortune points");
          }
        }

        if ( action === "fortune-successlevel" ) {

          if (actor.data.data.status.fortune.value > 0) {

            // reduce actor fortune by one
            actor.data.data.status.fortune.value = actor.data.data.status.fortune.value -1;

            // derive data to return to actor.rollSimpleTest method.
            let statId = card.attr('data-stat-id'),
                statType = card.attr('data-stat-type'),
                difficulty = card.attr('data-difficulty-id'),
                rollModifier = card.attr('roll-modifier'),
                result = button.attr("data-result");

            let reRenderData = {
              messageId: messageId,
              difficulty: difficulty,          
              actor: actor,
              action: action,
              result: result,
              rollModifier : rollModifier,
              fortune: true,
              reroll: false,
              addSuccessLevel: true
            }
            
            actor.rollSimpleTest(statType, statId, true, reRenderData);
          } else {
            console.log("no fortune points");
          }
        }
        
        // Get the item
        if ( !actor ) return;
        let itemData = actor.items.find(i => i.id === itemId);
        if ( !itemData ) return;
        let item = new ItemWfrp4e(itemData, actor);
  
        // Opposed Melee Test
        if ( action === "attackersTest" ) item.rollAttackersTest(ev);
  
        // Consumable usage
        else if ( action === "consume" ) item.useConsumable(ev);
  
        // Tool usage
        else if ( action === "toolCheck" ) item.toolCheck(ev);
      });
  
      // Dice roll context
      new ContextMenu(html, ".dice-roll", {
        "Apply Damage": {
          icon: '<i class="fas fa-user-minus"></i>',
          callback: event => this.applyDamage(event, 1)
        },
        "Apply Healing": {
          icon: '<i class="fas fa-user-plus"></i>',
          callback: event => this.applyDamage(event, -1)
        },
        "Double Damage": {
          icon: '<i class="fas fa-user-injured"></i>',
          callback: event => this.applyDamage(event, 2)
  
        },
        "Half Damage": {
          icon: '<i class="fas fa-user-shield"></i>',
          callback: event => this.applyDamage(event, 0.5)
        }
      });
    } 
  
    /* -------------------------------------------- */
  
    static applyDamage(event, multiplier) {
      let roll = $(event.currentTarget).parents('.dice-roll'),
          value = Math.floor(parseFloat(roll.find('.dice-total').text()) * multiplier);
  
      // Get tokens to which damage can be applied
      const tokens = canvas.tokens.controlledTokens.filter(t => {
        if ( t.actor && t.data.actorLink ) return true;
        else if ( t.data.bar1.attribute === "attributes.hp" || t.data.bar2.attribute === "attributes.hp" ) return true;
        return false;
      });
      if ( tokens.length === 0 ) return;
  
      // Apply damage to all tokens
      for ( let t of tokens ) {
        if ( t.actor && t.data.actorLink ) {
          let hp = parseInt(t.actor.data.data.attributes.hp.value),
              max = parseInt(t.actor.data.data.attributes.hp.max);
          t.actor.update({"data.attributes.hp.value": Math.clamped(hp - value, 0, max)}, true);
        }
        else {
          let bar = (t.data.bar1.attribute === "attributes.hp") ? "bar1" : "bar2";
          t.update({[`${bar}.value`]: Math.clamped(t.data[bar].value - value, 0, t.data[bar].max)}, true);
        }
      }
    }
  }
  
  
  /* -------------------------------------------- */
  
  
  // Activate global listeners
  //Hooks.on('renderChatLog', html => ItemWfrp4e.chatListeners(html));
  Hooks.on('renderChatLog', (log, html, data) => ItemWfrp4e.chatListeners(html));
  
  // Assign ItemWfrp4e class to CONFIG
  CONFIG.Item.entityClass = ItemWfrp4e;
  
  
  /* -------------------------------------------- */
  
  
  /**
   * Override and extend the basic :class:`ItemSheet` implementation
   */
  class ItemWfrp4eSheet extends ItemSheet {
    constructor(item, options) {
      super(item, options);
      this.mce = null;
    }
  
    /* -------------------------------------------- */
  
    /**
     * Use a type-specific template for each different item type
     */
    get template() {
      let type = this.item.type;
      return `public/systems/wfrp4e/templates/items/item-${type}-sheet.html`;
    }
  
    /* -------------------------------------------- */
  
    /**
     * Prepare item sheet data
     * Start with the base item data and extending with additional properties for rendering.
     */
    getData() {
      const data = super.getData();
      data['abilities'] = game.system.template.actor.data.abilities;
      data['damageTypes'] = CONFIG.damageTypes;
      let types = (this.item.type === "equipment") ? "armorTypes" : this.item.type + "Types";
      data[types] = CONFIG[types];
      if ( this.item.type === "talent" ) {
        data["talentMaxAdvances"] = CONFIG.talentMaxAdvances;
      }
      if ( this.item.type === "skill" ) {
        data["attributeSelect"] = CONFIG.attributeSelect;
      }
      if ( this.item.type === "spell" ) {
        data["spellSchools"] = CONFIG.spellSchools;
        data["spellLevels"] = CONFIG.spellLevels;
      }
      return data;
    }
  
    /* -------------------------------------------- */
  
    /**
     * Activate listeners for interactive item sheet events
     */
    activateListeners(html) {
      super.activateListeners(html);
  
        // Activate TinyMCE Editors
        html.find(".editor a.editor-edit").click(ev => {
          let button = $(ev.currentTarget),
              editor = button.siblings(".editor-content");
          createEditor({
          target: editor[0],
          height: editor.parent().height() - 40,
          save_enablewhendirty: true,
          save_onsavecallback: ed => this._onSaveMCE(ed, editor.attr("data-edit"))
        }).then(ed => {
          this.mce = ed[0];
          button.hide();
          this.mce.focus();
        });
      });
  
      // Activate tabs
      html.find('.tabs').each((_, el) => new Tabs(el));
    }
  
    /* -------------------------------------------- */
  
    /**
     * Customize sheet closing behavior to ensure we clean up the MCE editor
     */
    close() {
      super.close();
      if ( this.mce ) this.mce.destroy();
    }
  
    /* -------------------------------------------- */
    /*  Saving and Submission                       */
    /* -------------------------------------------- */
  
    /**
     * Handle using the Save button on the MCE editor
     * @private
     */
    _onSaveMCE(ed, target) {
      const form = this.element.find('.item-sheet')[0];
      const itemData = validateForm(form);
      itemData[target] = ed.getContent();
  
      // Update owned items
      if (this.item.isOwned) {
        itemData.id = this.item.data.id;
        this.item.actor.updateOwnedItem(itemData, true);
        this.render(false);
      }
  
      // Update unowned items
      else {
        this.item.update(itemData, true);
        this.render(false);
      }
  
      // Destroy the editor
      ed.remove();
      ed.destroy();
    }
  }
  
  
  /* -------------------------------------------- */
  
  
  // Override CONFIG
  CONFIG.Item.sheetClass = ItemWfrp4eSheet;
  
  // Standard D&D Damage Types
  CONFIG.damageTypes = {
    "acid": "Acid",
    "bludgeoning": "Bludgeoning",
    "cold": "Cold",
    "fire": "Fire",
    "force": "Force",
    "lightning": "Lightning",
    "necrotic": "Necrotic",
    "piercing": "Piercing",
    "poison": "Poison",
    "psychic": "Psychic",
    "radiant": "Radiant",
    "slashing": "Slashing",
    "thunder": "Thunder",
    "healing": "Healing"
  };
  
  // Weapon Types
  CONFIG.weaponTypes = {
    "simpleM": "Simple Melee",
    "simpleR": "Simple Ranged",
    "martialM": "Martial Melee",
    "martialR": "Martial Ranged",
    "natural": "Natural",
    "improv": "Improvised",
    "ammo": "Ammunition"
  };
  
  // Weapon Properties
  CONFIG.weaponProperties = {
    "thr": "Thrown",
    "amm": "Ammunition",
    "fir": "Firearm",
    "rel": "Reload",
    "two": "Two-Handed",
    "fin": "Finesse",
    "lgt": "Light",
    "ver": "Versatile",
    "hvy": "Heavy",
    "rch": "Reach"
  };
  
  // Equipment Types
  CONFIG.armorTypes = {
    "clothing": "Clothing",
    "light": "Light Armor",
    "medium": "Medium Armor",
    "heavy": "Heavy Armor",
    "bonus": "Magical Bonus",
    "natural": "Natural Armor",
    "shield": "Shield"
  };
  
  // Consumable Types
  CONFIG.consumableTypes = {
    "potion": "Potion",
    "scroll": "Scroll",
    "wand": "Wand",
    "rod": "Rod",
    "trinket": "Trinket"
  };
  
  // Spell Types
  CONFIG.spellTypes = {
    "attack": "Spell Attack",
    "save": "Saving Throw",
    "heal": "Healing",
    "utility": "Utility"
  };
  
  // Spell Schools
  CONFIG.spellSchools = {
    "abj": "Abjuration",
    "con": "Conjuration",
    "div": "Divination",
    "enc": "Enchantment",
    "evo": "Evocation",
    "ill": "Illusion",
    "nec": "Necromancy",
    "trs": "Transmutation",
  };
  
  // Spell Levels
  CONFIG.spellLevels = {
    0: "Cantrip",
    1: "1st Level",
    2: "2nd Level",
    3: "3rd Level",
    4: "4th Level",
    5: "5th Level",
    6: "6th Level",
    7: "7th Level",
    8: "8th Level",
    9: "9th Level"
  };
  
  // Talent Maxs
  CONFIG.talentMaxAdvances = {
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "ws": "Weapon Skill Bonus",
    "bs": "Ballistic Skill Bonus",
    "s": "Strength Bonus",
    "t": "Toughness Bonus",
    "i": "Initiative Bonus",
    "ag": "Agility Bonus",
    "dex": "Dexterity Bonus",
    "int": "Intelligence Bonus",
    "wp": "Willpower Bonus",
    "fel": "Fellowship Bonus"
  };

    // Attributes
    CONFIG.attributeSelect = {
      "ws": "Weapon Skill",
      "bs": "Ballistic Skill",
      "s": "Strength",
      "t": "Toughness",
      "i": "Initiative",
      "ag": "Agility",
      "dex": "Dexterity",
      "int": "Intelligence",
      "wp": "Willpower",
      "fel": "Fellowship"
    };

  // Melee Skills
  CONFIG.meleeSkills = {
    "meleeBasic": "Melee (Basic)",
    "meleeBrawling": "Melee (Brawling)",
    "meleeCavalry": "Melee (Cavalry)",
    "meleeFencing": "Melee (Fencing)",
    "meleeFlail": "Melee (Flail)",
    "meleeParry": "Melee (Parry)",
    "meleePoleArm": "Melee (PoleArm)",
    "meleeTwoHanded": "Melee (TwoHanded)"
  };

  
  // Melee Skills
  CONFIG.difficulty = {
    "60": "(+60) Very Easy",
    "40": "(+40) Easy",
    "20": "(+20) Average",
    "0": "(+0) Challenging",
    "-10": "(-10) Difficult",
    "-20": "(-20) Hard",
    "-30": "(-30) Very Hard"
  }