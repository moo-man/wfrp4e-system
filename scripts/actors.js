/**
 * Extend the base Actor class to implement additional logic specialized for D&D5e.
 */
class ActorWfrp4e extends Actor {
  prepareData(actorData) {
    actorData = super.prepareData(actorData);
    const data = actorData.data;

    // Calculate Attribute Values
    for (let abl of Object.values(data.attributes)) {
      abl.value = parseFloat(abl.value || 0);
      abl.talents = parseFloat(abl.talents || 0);
      abl.advances = parseFloat(abl.advances || 0);
      abl.modifier = parseFloat(abl.modifier || 0);
      abl.total = abl.value + abl.talents + abl.advances + abl.modifier;
      abl.bonus = Math.floor(abl.total / 10);
    }

    // Calculate Skill Values
    for (let skl of Object.values(data.skills)) {
      skl.advances = parseFloat(skl.advances || 0);
      skl.modifier = parseFloat(skl.modifier || 0);
      skl.style = "untrained";
      skl.trained = false;
      if (skl.advances > 0) {
        skl.style = "trained";
        skl.trained = true;
      }
      let abl = parseFloat(data.attributes[skl.ability].total || 0);
      skl.total = abl + skl.advances + skl.modifier;
    }

    //Calculate Move Values
    data.details.move.value = parseFloat(data.details.move.value || 0);
    data.details.move.walk = data.details.move.value * 2;
    data.details.move.run = data.details.move.value * 4;

    //derive 'item' attirbutes
    const talentlist = {};
    const skilllist = {};

    //Talent Placeholders
    let species = actorData.data.details.species.value;
    if (species) {
      talentlist[species] = talentlist[species] || {
        label: species,
        talents: []
      };
    }
    //career Placeholders
    let career = actorData.data.details.career.value;
    if (career) {
      talentlist[career] = talentlist[career] || {
        label: career,
        talents: []
      };
    }

    for ( let i of actorData.items ) {
      
      // Talents
      if ( i.type === "talent" ) {
        let career = i.data.career.value || species;
        talentlist[career] = talentlist[career] || {
          label: career,
          talents: []
        };
        if (i.data.max.value == null) {
          i.data.max.total = 1;
        } else if ((parseFloat(i.data.max.value)) > 0) {
          i.data.max.total = parseFloat(i.data.max.value);
        } else {
          i.data.max.total = actorData.data.attributes[i.data.max.value].bonus || 0; 
        }
        talentlist[career].talents.push(i);
      } 

      // Advanced Skills
      if ( i.type === "skill" ) {
        skilllist["advanced"] = skilllist["advanced"] || {
          label: "advanced",
          skills: []
        };
        let abl = parseFloat(actorData.data.attributes[i.data.attribute.value].total || 0);
        i.data.total.value = abl + i.data.advances.value + i.data.modifier.value;
        skilllist["advanced"].skills.push(i);
      } 

      actorData.talentlist = talentlist;
      actorData.skilllist = skilllist;
    }


    // Return the prepared Actor data
    return actorData;
  }  

  /**
   * Roll Simple Attribute Test
   * Holding SHIFT when the test is rolled will "fast-forward".
   * This chooses the default options of difficulty
   */
  rollSimpleTest(statType, statId, reRender=false, rerollData) {
    // Get data
    let rollData = {},
        stat = {},
        flavor,
        dialog = true,
        rollSound;

    if (statType == "attribute") stat = this.data.data.attributes[statId];
    else if (statType == "skill") stat = this.data.data.skills[statId];
    else if (statType == "advancedSkill") {
      let itemData = this.data.items.find(i => i.id == statId);
      stat.label = itemData.name;
      stat.total = itemData.data.total.value;
    }
    
    // check if we are rerendering a previous roll card and modify/inherit rollData appropriately.
    if (reRender) {
      dialog = false;
      rollData.difficulty = parseFloat(rerollData.difficulty || 0);
      rollData.rollModifier = parseFloat(rerollData.rollModifier || 0);
      if (rerollData.addSuccessLevel) {
        flavor = `${stat.label} - Simple Test (Add SL)`;
        rollSound = CONFIG.sounds.notification;
      } else if (rerollData.reroll) {
        flavor = `${stat.label} - Simple Test (Reroll)`;
        rollSound = CONFIG.sounds.dice;
      }
    } else {
      flavor = `${stat.label} - Simple Test`;
      rollData.difficulty = 0;
      rollData.rollModifier = 0;
      rollSound = CONFIG.sounds.dice;
    };   
    
    mergeObject(
      rollData,
      { 
        actor: this.data,
        flavor: flavor,
        stat: stat,
        statId: statId,
        statType: statType,
        rollTarget: 0,
        slModifier: 0,
        critical: false,
        automatic: false,
        reRender: reRender,
        rerollData: rerollData
      }, 
    ); 

    let chatOptions = {
      user: game.user._id,
      alias: this.name,      
      template: "public/systems/wfrp4e/templates/roll/simple-test.html",
      sound: rollSound,
      };

    // Fast-forward rolls (holding down shift or control will supress the difficulty dialog)
    if ( [KEYS.SHIFT, KEYS.CTRL].some(k => keyboard.isDown(k)) ) {
      rollData.difficulty = 0;
      dialog = false;
    }

    // Calculate rollTarget and difficulty description text.
    rollData.rollTarget = rollData.stat.total + rollData.difficulty + rollData.rollModifier;
    let difficultyList = CONFIG.difficulty;
    rollData.difficultyDescription = difficultyList[rollData.difficulty];

    if (dialog) {

      renderTemplate('public/systems/wfrp4e/templates/roll/attribute-dialog.html', this).then(dlg => {

        // Create a dialog
          new Dialog({
            title: flavor,
            content: dlg,
            buttons: {
              create: {
                label: "Roll",
                callback: dlg => {
                  // Calculate difficulty based on form data
                  const form = dlg.find('#attribute-dialog');
                  const data = validateForm(form[0]);
                  rollData.difficulty = parseFloat(data.difficulty || 0);
                  rollData.rollModifier = parseFloat(data.rollModifier || 0)

                  //update roll target based on new difficulty
                  rollData.rollTarget = rollData.stat.total + rollData.difficulty + rollData.rollModifier;
                  rollData.difficultyDescription = difficultyList[rollData.difficulty];
                }
              }
    
            },
            close: html => {
              // New roll using options from the dialig
              rollData = this.executeRoll(rollData);
              this.renderRollCard(chatOptions, rollData);
            }
          }).render(true);
        }); 
      } else if (reRender) {
        // Roll rerendering an existing chat card
        if (rerollData.reroll) rollData = this.executeRoll(rollData);
        else if (rerollData.addSuccessLevel) rollData = this.addSuccessLevel(rollData, rerollData.result);

        this.reRenderRollCard(chatOptions, rollData, rollData.rerollData.messageId);
      } else {
        // Fast-forward roll
        rollData = this.executeRoll(rollData);
        this.renderRollCard(chatOptions, rollData);
      }
  }

  /**
   * Executes a d100 roll based on the roll data provided and calculates results
   */
  executeRoll(rollData) {  
        
    // Define roll function
    let roll = () => {
      let roll = new Roll("1d100", rollData);
      roll.roll();
      rollData.result = roll.result;
    };
    
    // Roll
    roll();    

    // Calculate data to render
    let success = false,
        successLevel = (Math.floor(rollData.rollTarget /10) - Math.floor(rollData.result / 10)) + rollData.slModifier;

    // Calculate success based on success level
    if (successLevel < 0) success = false;
    else if (successLevel > 0) success = true;
    else {
      if (rollData.result <= rollData.rollTarget) success = true;
    }

    mergeObject(
      rollData,
      { 
        slTotal: successLevel,
        success: success
      }
    );

    return rollData;    
  }

  /**
   * Takes an existing roll (from roll data) and adds a successlevel then calculates if that changes the result.
   */
  addSuccessLevel(rollData, previousRoll) {  
        
    rollData.result = previousRoll;
    let addSL = 1;
    
    // Calculate data to render
    let success = false,
        successLevel = (Math.floor(rollData.rollTarget /10) - Math.floor(rollData.result / 10)) + rollData.slModifier + addSL;

    // Calculate success based on success level
    if (successLevel < 0) success = false;
    else if (successLevel > 0) success = true;
    else {
      if (rollData.result <= rollData.rollTarget) success = true;
    }

    mergeObject(
      rollData,
      { 
        slTotal: successLevel,
        success: success
      }
    );

    return rollData;    
  }

  /**
   * Renders rollData to a chat card
   */
  renderRollCard(chatOptions, rollData) {


    // Generate HTML from the requested chat template
    return renderTemplate(chatOptions.template, rollData).then(html => {
      
      // Emit the HTML as a chat message
      chatOptions["content"] = html;
      //ChatMessage.create(chatOptions, true);
      ChatMessage.create({
        user: game.user._id,
        alias: this.data.name,
        content: html
      }, false);
        
      // Play a sound effect
      //if ( chatOptions.sound ) Audio.play({src: chatOptions.sound}, true);
      AudioHelper.play({src: chatOptions.sound, volume: 0.8});
      return html;
    });
  }

  /**
   * Re-renders an exisitng chat card with new content (based on rollData)
   */
  reRenderRollCard(chatOptions, rollData, messageId=null) {
    // Generate HTML from the requested chat template
    return renderTemplate(chatOptions.template, rollData).then(html => {
      
      // Find index of message card to reroll.
      let index = game.messages.entities.findIndex(e => e.data._id === messageId);
      
      // Update message card with new HTML content.
      let m = game.messages.entities[index];
      m.update({content: html}, true).then(message => {
        ui.chat.updateMessage(message);
      });
        
      // Play a sound effect
      //if ( chatOptions.sound ) Audio.play({src: chatOptions.sound}, true);
      AudioHelper.play({src: chatOptions.sound, volume: 0.8});
      return html;
    });
  }
}

CONFIG.Actor.entityClass = ActorWfrp4e;

/**
 * Extend the basic ActorSheet with some very simple modifications
 */
class ActorSheetWfrp4e extends ActorSheet {
  /**
   * Add a popouts attribute to the class to track popouts from this main sheet for the purposes of rendering them together.
   */
  constructor(actor, options) {
    super(actor, options);
    this._popouts = [];
  }
  /**
   * Customize the template path which is rendered for the Actor Sheet.
   */
  get template() {
    return "public/systems/wfrp4e/templates/actors/actor-sheet.html";
  }

  /**
   * Customize the data object which is provided to the template to be rendered.
   */
  getData() {
    const sheetData = super.getData();

    // Prepare owned items
    this._prepareItems(sheetData.actor);

    //return super.getData();
    return sheetData;
  }

  _prepareItems(actorData) {

    // Inventory
    const inventory = {
      weapon: { label: "Weapons", items: [] },
      armour: { label: "Armour", items: [] },
      container: { label: "Packs and Containers", items: [] }
    };

    // Spellbook
    const spellbook = {};
    const talentlist = {};
    const skilllist = {};

    //Talent Placeholders
    let species = actorData.data.details.species.value;
    if (species) {
      talentlist[species] = talentlist[species] || {
        label: species,
        talents: []
      };
    }
    let career = actorData.data.details.career.value;
    if (career) {
      talentlist[career] = talentlist[career] || {
        label: career,
        talents: []
      };
    } 
    
    // Iterate through items, allocating to containers
    let totalEncumbrance = 0;
    for ( let i of actorData.items ) {
      i.img = i.img || DEFAULT_TOKEN;

      // Inventory
      if ( Object.keys(inventory).includes(i.type) ) {
        i.data.quantity.value = i.data.quantity.value || 1;
        i.data.encumbrance.value = i.data.encumbrance.value || 0;
        i.totalEncumbrance = Math.round(i.data.quantity.value * i.data.encumbrance.value * 10) / 10;
        inventory[i.type].items.push(i);
        totalEncumbrance += i.totalEncumbrance;
      }

      // Talents
      if ( i.type === "talent" ) {
        let career = i.data.career.value || species;
        talentlist[career] = talentlist[career] || {
          label: career,
          talents: []
        };
        if (i.data.max.value == null) {
          i.data.max.total = 1;
        } else if ((parseFloat(i.data.max.value)) > 0) {
          i.data.max.total = parseFloat(i.data.max.value);
        } else {
          i.data.max.total = actorData.data.attributes[i.data.max.value].bonus || 0; 
        }
        talentlist[career].talents.push(i);
      } 

      // Advanced Skills
      if ( i.type === "skill" ) {
        skilllist["advanced"] = skilllist["advanced"] || {
          label: "advanced",
          skills: []
        };
        i.data.icon.value = this._getCareerIcon(i.data.career.value);
        let abl = parseFloat(actorData.data.attributes[i.data.attribute.value].total || 0);
        i.data.total.value = abl + i.data.advances.value + i.data.modifier.value;
        skilllist["advanced"].skills.push(i);
      } 
    }

    // Assign and return
    actorData.inventory = inventory;
    actorData.spellbook = spellbook;
    actorData.talentlist = talentlist;
    actorData.skilllist = skilllist;

    return actorData;

  }

  /**
   * Get the font-awesome icon used to display a certain level of skill proficiency
   * @private
   */
  _getCareerIcon(level) {
    const icons = {
      0: '<i class="far fa-circle"></i>',
      false: '<i class="far fa-circle"></i>',
      1: '<i class="fas fa-check"></i>',
      true: '<i class="fas fa-check"></i>',
    };
    return icons[level];
  }

  /**
   * Configure the actor sheet so that when ever it is rendered. It also renders any child pop-outs that are recorded in the _popouts attribute.
   */
  render(force=false) {
    // Render the Actor Sheet as normal
   super.render(force);
   
   // Render any open pop-out sheets
   for ( let p of this._popouts ) {
     p.render();
   }
 }


  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
	activateListeners(html) {
    super.activateListeners(html);

    // Pad field width
    html.find('[data-wpad]').each((i, e) => {
      let text = e.tagName === "INPUT" ? e.value : e.innerText,
        w = text.length * parseInt(e.getAttribute("data-wpad")) / 2;
      e.setAttribute("style", "flex: 0 0 " + w + "px");
    });

    // Activate tabs
    html.find('.tabs').each((_, el) => {
      let tabs = $(el),
        initial = this.actor.data.flags["_sheetTab-" + tabs.attr("data-tab-container")];
      new Tabs(tabs, initial, clicked => {
        this.actor.data.flags["_sheetTab-" + clicked.parent().attr("data-tab-container")] = clicked.attr("data-tab");
      });
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Activate TinyMCE Editors
    html.find(".editor a.editor-edit").click(ev => {
      let button = $(ev.currentTarget),
        editor = button.siblings(".editor-content");
      let mce = createEditor({
        target: editor[0],
        height: editor.parent().height() - 40,
        save_enablewhendirty: true,
        save_onsavecallback: ed => {
          let target = editor.attr("data-edit");
          this.actor.update({[target]: ed.getContent()}, true);
          ed.remove();
          ed.destroy();
        }
      }).then(ed => {
        button.hide();
        ed[0].focus();
      });
    });

    /* -------------------------------------------- */
    /*  Attributes and Skills
    /* -------------------------------------------- */

    // Attribute Career
    html.find('.attribute-career').click(ev => {
      let field = $(ev.currentTarget).siblings('input[type="hidden"]');
      field.val(1 - field.val());
      let formData = validateForm(field.parents('form')[0]);
      this.actor.update(formData, true);
    });

    // Attributes Checks
    html.find('.attribute-name').click(ev => {
      let abl = ev.currentTarget.parentElement.getAttribute("data-attribute");
      //this.actor.rollAbility(abl);
      this.actor.rollSimpleTest("attribute", abl);
    });

    // Modify Attributes
    html.find('.modify-attributes').click(ev => {
      this.modifyAtributes(ev);
    });

    // Roll Skill Checks
    // This should be updated to be the consistent with how advanced skill checks are performed. (using the item.roll method)
    html.find('.skill-name').click(ev => {
      let skl = ev.currentTarget.parentElement.getAttribute("data-skill");
      this.actor.rollSimpleTest("skill", skl);
    });

    // Modify Skills
    html.find('.modify-skills').click(ev => {
      this.modifySkills(ev);
    });

    /* -------------------------------------------- */
    /*  Rollable Items                              */
    /* -------------------------------------------- */

    html.find('.item .rollable').click(ev => {
      console.log("ev: ", ev)
      let itemId = Number($(ev.currentTarget).parents(".item").attr("data-item-id")),
        Item = CONFIG.Item.entityClass,
        item = new Item(this.actor.items.find(i => i.id === itemId), this.actor);

      if (item.data.type == "skill") this.actor.rollSimpleTest("advancedSkill", item.data.id);
      else item.roll();
    });

    /* -------------------------------------------- */
    /*  Inventory
    /* -------------------------------------------- */

    // Create New Item
    html.find('.item-create').click(ev => {
      let type = ev.currentTarget.getAttribute("data-item-type");
      this.actor.createOwnedItem({name: "New " + type.capitalize(), type: type}, true);
    });

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      let itemId = Number($(ev.currentTarget).parents(".item").attr("data-item-id"));
      let Item = CONFIG.Item.entityClass;
      const item = new Item(this.actor.items.find(i => i.id === itemId), this.actor);
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      let li = $(ev.currentTarget).parents(".item"),
        itemId = Number(li.attr("data-item-id"));
      this.actor.deleteOwnedItem(itemId, true);
      li.slideUp(200, () => this.render(false));
    });
  }
  
  modifyAtributes(ev) {
    new ModifyAtributes(this, {width: 350, top: 80, left: window.innerWidth - 910}).render(true);
  } 

  modifySkills(ev) {
    new ModifySkills(this, {width: 704, top: 80, left: window.innerWidth - 910}).render(true);
  } 
}

/* class ModifyAtributesDialog extends Dialog {
  constructor(data, options, actor) {
      super(data, options);
      
      this.data = data;
      this.actor = actor;
  }

  activateListeners(html) {

    // Activate base class listeners
    super.activateListeners(html);

    html.find('.attribute-score').focusout(ev => {
        let field = ev.target,
            val = field.value;
        this.actor.update({[field.name]: val}, true);
    });
    html.find('.attribute-talents').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
    });
    html.find('.attribute-advances').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
    });
    html.find('.attribute-modifier').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
    });
    // Attribute Career
    html.find('.attribute-career').click(ev => {
      let field = $(ev.currentTarget).siblings('input[type="hidden"]');
      field.val(1 - field.val());
      let formData = validateForm(field.parents('form')[0]);
      console.log(formData);
      this.actor.update(formData, true);
    });
  }
} */

class ModifyAtributes extends Application {
  constructor(actorSheet, options) {
      super(options);
      
      // Store a reference to the passed Actor
      this.actor = actorSheet.actor;

      // Push a reference to this new ModifySkills pop-out to the main actorSheet _popouts attribute.
      actorSheet._popouts.push(this)
  }

  /**
   * Assign the default options which are supported by the CharacterSelect menu
   */
  static get defaultOptions() {
    const options = super.defaultOptions;
    options.template = "public/systems/wfrp4e/templates/actors/modify-abilities.html";
    return options;
  }

  /**
   * Give the window a dynamic title which depends on the referenced Actor
   */
  get title() {
    return `${this.actor.name} - Modify Abilities`;
  }

  /**
   * Provide data to the application to render
   */
  getData() {
    
    // Attribute career
    for ( let abl of Object.values(this.actor.data.data.attributes)) {
      abl.career = parseFloat(abl.career || 0);
      abl.icon = this._getCareerIcon(abl.career);
      abl.hover = this._getCareerHover(abl.career);
    }

    return this.actor.data;
  }

  /**
   * Get the font-awesome icon used to display a certain level of skill proficiency
   * @private
   */
  _getCareerIcon(level) {
    const icons = {
      0: '<i class="far fa-circle"></i>',
      false: '<i class="far fa-circle"></i>',
      1: '<i class="fas fa-check"></i>',
      true: '<i class="fas fa-check"></i>',
    };
    return icons[level];
  }

  /* -------------------------------------------- */

  /**
   * Get the hover text used to display a certain level of skill proficiency
   * @private
   */
  _getCareerHover(level) {
    return {
      0: "Non-Career Attribute",
      1: "Career Attribute",
    }[level];
  }

  /**
   * Activate HTML Listeners
   */
  activateListeners(html) {

      // Activate base class listeners
      super.activateListeners(html);

    // Handle attribute score changes
    html.find('.attribute-score').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
      this.render();
    });
    html.find('.attribute-talents').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
      this.render();
    });    
    html.find('.attribute-advances').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
      this.render();
    });
    html.find('.attribute-modifier').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
      this.render();
    });
    // Attribute Career
    html.find('.attribute-career').click(ev => {
      let field = $(ev.currentTarget).siblings('input[type="hidden"]');
      field.val(1 - field.val());
      let formData = validateForm(field.parents('form')[0]);
      this.actor.update(formData, true);
      this.render();
    });
  }
}

class ModifySkills extends Application {
  constructor(actorSheet, options) {
      super(options);
      
      // Store a reference to the passed Actor
      this.actor = actorSheet.actor;

      // Push a reference to this new ModifySkills pop-out to the main actorSheet _popouts attribute.
      actorSheet._popouts.push(this)
  }

  /**
   * Assign the default options which are supported by the CharacterSelect menu
   */
  static get defaultOptions() {
    const options = super.defaultOptions;
    options.template = "public/systems/wfrp4e/templates/actors/modify-skills.html";
    return options;
  }

  /**
   * Give the window a dynamic title which depends on the referenced Actor
   */
  get title() {
    return `${this.actor.name} - Modify Skills`;
  }

  /**
   * Provide data to the application to render
   */
  getData() {

    // Attribute career
    for ( let skl of Object.values(this.actor.data.data.skills)) {
      skl.career = parseFloat(skl.career || 0);
      skl.icon = this._getCareerIcon(skl.career);
      skl.hover = this._getCareerHover(skl.career);
    }

    this._prepareItems(this.actor);

    //return this.actor.data;
    return this.actor;
  }

  _prepareItems(actorData) {

    // Itemlists
    const skilllist = {};

    // Iterate through items, allocating to containers
    for ( let i of actorData.items ) {
      i.img = i.img || DEFAULT_TOKEN;

      // Advanced Skills
      if ( i.type === "skill" ) {
        skilllist["advanced"] = skilllist["advanced"] || {
          label: "advanced",
          skills: []
        };
        i.data.icon.value = this._getCareerIcon(i.data.career.value);
        let abl = parseFloat(actorData.data.data.attributes[i.data.attribute.value].total || 0);
        i.data.total.value = abl + i.data.advances.value + i.data.modifier.value;
        skilllist["advanced"].skills.push(i);
      } 
    }

    // Assign and return
    actorData.skilllist = skilllist;
  }

  /**
   * Get the font-awesome icon used to display a certain level of skill proficiency
   * @private
   */
  _getCareerIcon(level) {
    const icons = {
      0: '<i class="far fa-circle"></i>',
      false: '<i class="far fa-circle"></i>',
      1: '<i class="fas fa-check"></i>',
      true: '<i class="fas fa-check"></i>',
    };
    return icons[level];
  }

  /* -------------------------------------------- */

  /**
   * Get the hover text used to display a certain level of skill proficiency
   * @private
   */
  _getCareerHover(level) {
    return {
      0: "Non-Career Skill",
      1: "Career Skill",
    }[level];
  }

  /**
   * Activate HTML Listeners
   */
  activateListeners(html) {

    // Activate base class listeners
    super.activateListeners(html);

    // Handle skill score changes
    html.find('.skill-advances').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
      this.render();
    });
    html.find('.skill-modifier').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
      this.render();
    });
    html.find('.skill-name').focusout(ev => {
      let field = ev.target,
          val = field.value;
      this.actor.update({[field.name]: val}, true);
      this.render();
    });
    // Skill Career
    html.find('.skill-career').click(ev => {
      let field = $(ev.currentTarget).siblings('input[type="hidden"]');
      field.val(1 - field.val());
      let formData = validateForm(field.parents('form')[0]);
      //console.log(formData);
      this.actor.update(formData, true);
      this.render();
    }); 

    /* -------------------------------------------- */
    /*  Inventory
    /* -------------------------------------------- */

    // Create New Item
    html.find('.item-create').click(ev => {
      let type = ev.currentTarget.getAttribute("data-item-type");
      this.actor.createOwnedItem({name: "New " + type.capitalize(), type: type}, true);
      this.render();
    });

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      let itemId = Number($(ev.currentTarget).parents(".item").attr("data-item-id"));
      let Item = CONFIG.Item.entityClass;
      const item = new Item(this.actor.items.find(i => i.id === itemId), this.actor);
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      let li = $(ev.currentTarget).parents(".item"),
        itemId = Number(li.attr("data-item-id"));
      this.actor.deleteOwnedItem(itemId, true);
      li.slideUp(200, () => this.render(false));
    });
    
  }
}


CONFIG.Actor.sheetClass = ActorSheetWfrp4e;

